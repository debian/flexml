#!/usr/bin/env perl

# Assume ARGV is ($basename @REST).  Print "passed" or "failed" based on the
# following comparisons:

#   Compare $basename.stderr with $basename.stderr.expected
#      pass if no difference or
#        $basename.stderr is empty and $basename.stderr.expected doesn't exist;
#   Compare $basename.stdout with $basename.stdout.expected
#      pass if no difference or
#        $basename.stdout is empty and $basename.stdout.expected doesn't exist;
#   For each file $foo in @REST
#   Compare $foo with $foo.expected
#      pass if no difference;


# Print "passed" if all tests pass, otherwise print
#       "failed <file-that-failed>"

# NOTES
#   In order to compare directories, GNU diff must be used. There
#   should probably be a test to ensure this.

# $Id: CompareOut.pl,v 1.2 2006/08/21 17:33:13 wdowling Exp $
# $Source: /cvsroot/flexml/flexml/testbed/CompareOut.pl,v $

use strict;
use Getopt::Std;

my %args;
getopts('p:', \%args);

my $diff_prog = $args{'p'} || 'diff';

my $diff_opt = "";
if ($ARGV[0] =~ /^-/) {
    $diff_opt = shift;
}

my $basename = shift;

my $diff_cmd = "$diff_prog $diff_opt";

my $retcode;
my $fail_file;
my $made_stdout = 0;
my $made_stderr = 0;

# Compare basename.stderr to basename.stderr.expected
if (! -f "$basename.stderr.expected") {
    system("touch $basename.stderr.expected");
    $made_stderr = 1;
}

$retcode =
  system("$diff_cmd $basename.stderr $basename.stderr.expected " .
	 "> $basename.stderr.diff 2>/dev/null");
system("rm -f $basename.stderr.expected") if $made_stderr;

$fail_file = "$basename.stderr" if $retcode;

# Compare basename.stdout to basename.stdout.expected
if ($retcode == 0) {
    if (! -f "$basename.stdout.expected") {
	system("touch $basename.stdout.expected");
	$made_stdout = 1;
    }

    $retcode =
      system("$diff_cmd $basename.stdout $basename.stdout.expected " .
	     "> $basename.stdout.diff 2>/dev/null");
    system("rm -f $basename.stdout.expected") if $made_stdout;

    $fail_file = "$basename.stdout" if $retcode;
}

while (($retcode == 0) && @ARGV) {
    my $file = shift;
    # compare file with file.expected
    $retcode = system("$diff_cmd $file $file.expected >$file.diff 2>/dev/null");
    $fail_file = $file if $retcode;
}

print ($retcode ? "failed $fail_file\n" : "passed $basename\n");
