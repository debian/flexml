/* header file to be included at end of generated %{ ... %} section */
#define YY_DECL static int static_lexer_param; int lexer(int lexer_param)
#define YY_USER_INIT static_lexer_param = lexer_param;
