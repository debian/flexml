#include <stdio.h>

#include "biparser-one.h"
#include "biparser-two.h"

/* XML application entry points. 
   Should be generated from two action files, but I wasn't succesfull in that area yet [Mt] */
void STag_one_foo(void) {}
void ETag_one_foo(void) {printf("foo pcdata: %s\n", one_pcdata);}
void STag_one_bar(void) {}
void ETag_one_bar(void) {printf("bar pcdata: %s\n", one_pcdata);}

void STag_two_toto(void) {}
void ETag_two_toto(void) {printf("toto pcdata: %s\n", two_pcdata);}
void STag_two_tutu(void) {}
void ETag_two_tutu(void) {printf("tutu pcdata: %s\n", two_pcdata);}

/* Parsers control.
   Should probably be added to the .h */
#ifndef YY_TYPEDEF_YY_BUFFER_STATE
#define YY_TYPEDEF_YY_BUFFER_STATE
typedef struct yy_buffer_state *YY_BUFFER_STATE;
#endif

extern int one_lex (void);
extern void one_restart (FILE *input_file  );
extern void one__switch_to_buffer (YY_BUFFER_STATE new_buffer  );
YY_BUFFER_STATE one__create_buffer (FILE *file,int size  );
extern void one__delete_buffer (YY_BUFFER_STATE b  );
extern void one__flush_buffer (YY_BUFFER_STATE b  );
extern void one_push_buffer_state (YY_BUFFER_STATE new_buffer  );
extern void one_pop_buffer_state (void );

extern int two_lex (void);
extern void two_restart (FILE *input_file  );
extern void two__switch_to_buffer (YY_BUFFER_STATE new_buffer  );
extern YY_BUFFER_STATE two__create_buffer (FILE *file,int size  );
extern void two__delete_buffer (YY_BUFFER_STATE b  );
extern void two__flush_buffer (YY_BUFFER_STATE b  );
extern void two_push_buffer_state (YY_BUFFER_STATE new_buffer  );
extern void two_pop_buffer_state (void );


int main(int argc, char **argv) {
	FILE *infile;
	YY_BUFFER_STATE buff;
	int retval;
	
	printf("Parse biparser-one.in\n");
	infile=fopen("biparser-one.in","r");
	buff=one__create_buffer(infile,10);
	one__switch_to_buffer(buff);
	retval = one_lex();
	one__delete_buffer(buff);
	fclose(infile);

   	printf("Parse biparser-two.in\n");
	infile=fopen("biparser-two.in","r");
	buff=two__create_buffer(infile,10);
	two__switch_to_buffer(buff);
	retval = two_lex() || retval;
	two__delete_buffer(buff);
	fclose(infile);

        return retval;
}



