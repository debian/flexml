# Make(1) rules for FleXML XML processor generator system.
# Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
#
# This file is part of the FleXML XML processor generator system.
# Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc., 59
# Temple Place, Suite 330, Boston, MA 02111-1307 USA.

# $Id: Makefile,v 1.9 2006/09/29 19:40:46 wdowling Exp $

# FILES.

include ../Makefile.defs

# Test the version in this dir, not the installed one
SKEL=../skel
ACT=../flexml-act

SAMPS =	my.dtd my-show.act my-joke.xml my-joke2.xml my-joke3.xml \
	tricky.dtd tricky.act tricky.xml \
	test.html xhtml1-transitional.dtd

SRC =	$(SAMPS)
ALL =	$(SAMPS)

.PHONY:		all install dist test clean

# PRIMARY TARGETS.

start: test

all:		$(ALL)

install:	$(ALL)
	mkdir -p $(DESTDIR)$(DOCDIR)/examples
	$(INSTALL) -m444 $(SAMPS) $(DESTDIR)$(DOCDIR)/examples/

dist:		clean
	#rsync -v FleXML.html $(WEBHOME)/FleXML.html
	#rsync -va --cvs-exclude --delete-excluded ./ $(FTPHOME)/

clean::; @echo "Cleaning examples..."
	$(RM) *.[olh1] *-dummy.? lex.* *~ ./#*

test::		all
	@echo "Testing..."


# DEFAULT RULES.
FLEXML_PROG = ../$(FLEXML) -s$(SKEL) -T$(ACT)

# Generate C source from flex scanner.
%.c: 		%.l
	$(FLEX) -B -s -v $(FLEXDEBUG) -o$@ $<

# Direct generation of stand-alone XML processor+application.
# Note: The dependency must be of the form "appl.l: appl.act proc.dtd".
%.l:		%.act
	$(FLEXML_PROG) $(FLEXDEBUG) -vA -a $^

# Generate XML processor to link with application.
%.l %.h:	%.dtd
	$(FLEXML_PROG) $(FLEXDEBUG) -v $<

# Generate XML application C source to compile and link with processor.
# Note: The dependency must be of the form "appl.c: appl.act proc.dtd".
%.c:		%.act
	$(FLEXML_PROG) $(FLEXDEBUG) -vD -a $^


clean::; $(RM) flexml-act flexml-act.c

# Example: LINK processor with application:

my.l my.h:	my.dtd
my.c:		my.l
my.o:		my.c my.h

my-show.c:	my-show.act my.dtd
my-show.o:	my-show.c my.h
my-show:	my-show.o my.o

test::		my-show
	@echo "XXXXX Test: LINK processor with application"
	if ./my-show <my-joke.xml  ; then echo "TEST PASSED"; else echo "TEST FAILED"; fi
	if ./my-show <my-joke2.xml ; then echo "TEST PASSED"; else echo "TEST FAILED"; fi
	if ./my-show <my-joke3.xml ; then echo "TEST FAILED"; else echo "TEST PASSED"; fi

clean::; $(RM) my.c my-show my-show.c my-dummy my-dummy.c

# Example: COMPILE processor into application

tricky.l:	tricky.act tricky.dtd
tricky.c:	tricky.l
tricky.o:	tricky.c
tricky:		tricky.o

test::		tricky
	@echo "XXXXX Test: COMPILE processor with application"
	if ./tricky <tricky.xml ; then echo "TEST PASSED"; else echo "TEST FAILED"; fi

clean::; $(RM) tricky.[lco] tricky

# Complex example: application to print XHTML <a href=...> values.

xhtml1-transitional.l xhtml1-transitional.h: xhtml1-transitional.dtd
	$(FLEXML_PROG) -rhtml \
		-p "-//IETF//DTD XHTML 1.0 Transitional//EN" \
		-u "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" \
		xhtml1-transitional.dtd

xhtml1-transitional.c:	xhtml1-transitional.l
	$(FLEX) -B -s -v -Ca -oxhtml1-transitional.c xhtml1-transitional.l

# turn off the -O2 when building this -- it takes too long to run
xhtml1-transitional.o: CFLAGS = -g

xhtml1-transitional.o:	xhtml1-transitional.c xhtml1-transitional.h

xhtml-href.c:	xhtml-href.act xhtml1-transitional.dtd
xhtml-href.o:	xhtml-href.c xhtml1-transitional.h
xhtml-href:	xhtml-href.o xhtml1-transitional.o

test::		xhtml-href FleXML.xml
	@echo "XXXXX Test: application to print XHTML <a href=...> values"
	if ./xhtml-href <test.html   ; then echo "TEST PASSED"; else echo "TEST FAILED"; fi
	if ./xhtml-href < FleXML.xml ; then echo "TEST PASSED"; else echo "TEST FAILED"; fi

clean::; $(RM) xhtml1-*.[lhco] xhtml-href.[co] xhtml-href

# END.

clean::; @echo "Done cleaning."
test::;	@echo "Done testing."
test:: clean

.PHONEY : echov

# on command line say 'make v=foo echov' to see the value
# of the variable foo
echov:
	@echo "$(subst ",\",$($(v)))";          #"# for emacs
