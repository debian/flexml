<!DOCTYPE html PUBLIC "-//IETF//DTD XHTML 1.0 Transitional//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--$Id: FleXML.xml,v 1.2 2006/07/18 18:21:13 mquinson Exp $-->
<html>
  <head>
    <title>FleXML - XML Processor Generator</title>
  </head>

  <body bgcolor='white'>

    <h1> FleXML - XML Processor Generator </h1>

    <blockquote><p><em>
	See also <a href="ftp/FleXML/flexml.html">the manual page</a>
	and a short <a href="ftp/FleXML/paper.html">white paper</a>.
	Or peek into the <a
	  href="http://www.ens-lyon.fr/~krisrose/ftp/FleXML/">master
	  source archive</a>.
    </em></p></blockquote>

    <p>
      FleXML reads a DTD (Document Type Definition)
      describing the format of XML (Extensible Markup Language)
      documents; it may be specified as a URI to the DTD on the
      web. From this FleXML produces a validating XML
      processor with an interface to support XML applications.  Proper
      applications can be generated optionally from special action
      files, either for linking or textual combination with the
      processor.
    </p>

    <p>
      FleXML is specifically developed for XML applications
      where a fixed data format suffices in the sense that a single
      DTD is used without individual extensions for a large number of
      documents.  (Specifically it restricts XML rule [28] to
    </p>

<pre>  [28r] doctypedecl ::= '<code>&lt;!DOCTYPE</code>' S Name S ExternalID S? '<code>&gt;</code>'
</pre>

    <p>
      where the ExternalId denotes the used DTD - one might say, in
      fact, that FleXML implements ``non-extensible'' markup. :)
    </p>

    <p>
      With this restriction we can do much better because the possible
      tags and attributes are static: FleXML-generated XML processors
      read the XML document character by character and can immediately
      dispatch the actions associated with each element (or reject the
      document as invalid).  Technically this is done by using the
      Flex scanner generator to produce a deterministic finite
      automaton with an element context stack for the DTD, which means
      that there is almost no overhead for XML processing.
    </p>

    <p>
      Furthermore we have devised a simple extension of the C
      programming language that facilitates the writing of `element
      actions' in C, making it easy to write really fast XML
      applications. In particular we represent XML attribute values
      efficiently in C when this is possible, thus avoiding the
      otherwise ubiquitous conversions between strings and data
      values.
    </p>

    <p>
      Compared to SAX and its XSL-based friends, FleXML immediately
      produces efficient code in that the interdiction of extension
      makes it possible to encode efficiently, FleXML for example uses
      native C `enum' types to implement enumeration attribute
      types. However, the above limitation does prevent uses in more
      complex settings.
    </p>

    <p>
      As an example: the following is all that is needed to produce a
      fast program that prints all href-attributes in
      <code>&lt;a...&gt;</code> tags in XHTML documents (and rejects
      invalid XHTML documents).
    </p>

<pre><code>  &lt;!DOCTYPE actions SYSTEM "flexml-act.dtd"&gt;
  &lt;actions&gt;
  &lt;top&gt;&lt;![CDATA[           #include &lt;stdio.h&gt;                  ]]&gt;&lt;/top&gt;
  &lt;start tag='a'&gt;&lt;![CDATA[ if ({href}) printf("%s\n", {href}); ]]&gt;&lt;/start&gt;
  &lt;/actions&gt;
</code></pre>

    <p>
      In general, action files are themselves XML documents conforming
      to the DTD
    </p>

<pre><code>   &lt;!ELEMENT actions ((top|start|end)*,main?)&gt;
   &lt;!ENTITY % C-code "(#PCDATA)"&gt;
   &lt;!ELEMENT top   %C-code;&gt;
   &lt;!ELEMENT start %C-code;&gt;  &lt;!ATTLIST start tag NMTOKEN #REQUIRED&gt;
   &lt;!ELEMENT end   %C-code;&gt;  &lt;!ATTLIST end   tag NMTOKEN #REQUIRED&gt;
   &lt;!ELEMENT main  %C-code;&gt;
</code></pre>

    <p>
      with <code>%C-code;</code> segments being in C enriched as
      described below.  The elements are used as follows:
    </p>

    <dl compact='compact'>
      <dt>top</dt>
      <dd><p>
	Use for top-level C code such as global declarations, utility
	functions, etc.
	</p></dd>

      <dt>start</dt>
      <dd>
	<p>
	  Attaches the code as an action to the element with the name
	  of the required ``tag'' attribute. The ``%C-code;''
	  component should be C code suitable for inclusion in a C
	  block (i.e., within {...} so it may contain local
	  variables); furthermore the following extensions are
	  available: {attribute} Can be used to access the value of
	  the attribute as set with attribute=value in the start
	  tag. In C, {attribute} will be interpreted depending on the
	  declaration of the attribute. If the attribute is declared
	  as an enumerated type like
	</p>

<pre><code>  &lt;!ATTLIST attrib (alt1 | alt2 |...) ...&gt;
</code></pre>

	<p>
	  then the C attribute value is of an enumerated type with the
	  elements written <code>{attrib=alt1}</code>,
	  <code>{attrib=alt2}</code>, etc.; furthermore an unset
	  attribute has the ``value'' <code>{!attrib}</code>. If the
	  attribute is not an enumeration then <code>{attrib}</code>
	  is a null-terminated C string (of type <code>char*</code>)
	  and <code>{!attrib}</code> is <code>NULL</code>.
	</p>
      </dd>

      <dt>end</dt>
      <dd>
	<p>
	  Similarly attaches the code as an action to the end tag with
	  the name of the required ``tag'' attribute; also here the
	  ``%C-code;'' component should be C code suitable for
	  inclusion in a C block. In case the element has ``Mixed''
	  contents, i.e, was declared to permit <code>#PCDATA</code>,
	  then the special variable <code>{#PCDATA}</code> contains
	  the text (<code>#PCDATA</code>) of the element as a
	  null-terminated C string (of type char*). In case the Mixed
	  contents element actually mixed text and child elements then
	  <code>{#PCDATA}</code> contains the plain concatenation of
	  the text fragments as one string.
	</p>
      </dd>

      <dt>main</dt>
      <dd>
	<p>
	  Finally, an optional ``main'' element can contain the C main
	  function of the XML application. Normally the main function
	  should include (at least) one call of the XML processor
	  <code>yylex</code>.
	</p>
      </dd>

    </dl>
    <p>
      The program is freely redistributable and modifiable (under GNU
      `copyleft').
    </p>
    <hr />
    <address>Copyright (c)
      <a href="mailto:Kristoffer.Rose@ens-lyon.fr">Kristoffer Rose</a>.
<!-- Created: Thu Dec  9 08:09:41 CET 1999 -->
<!-- hhmts start -->
Last modified: Mon Dec 13 17:17:44 CET 1999
<!-- hhmts end -->
    </address>
  </body>
</html>
