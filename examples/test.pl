#!/usr/bin/perl -w

         # Create a user agent object
         use LWP::UserAgent;
         $ua = new LWP::UserAgent;
         $ua->agent("Test/0.1 " . $ua->agent);

         # Create a request
         my $req = new HTTP::Request GET => 'file:test.pl';

         # Pass request to the user agent and get a response back
         my $res = $ua->request($req);

         # Check the outcome of the response
         if ($res->is_success) {
             print "Contents:\n" . $res->content;
         } else {
             print "Bad luck this time\n";
         }

