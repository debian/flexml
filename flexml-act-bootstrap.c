/* Flex(1) XML processor action language application.
 * Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
 * 
 * This file is part of the FleXML XML processor generator system.
 * Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdlib.h>
#include <stdio.h>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32) || defined(__TOS_WIN__)
#  ifndef __STRICT_ANSI__
#    include <io.h>
#    include <process.h>
#  endif
#else
#  include <unistd.h>
#endif

#include "flexml-act.h"

extern char *bufferstack;
extern FILE* yyin;
extern int yylineno;
extern int yylex(void);

char* filename;

void STag_top(void)
{
  printf("\n#line %d \"%s\"\n", yylineno, filename);
}
void ETag_top(void)
{
  printf("%s\n", pcdata);
}

const char* tag;

void STag_start(void)
{
  printf("void STag_%s(void)\n{", tag = A_start_tag);
  printf("\n#line %d \"%s\"\n", yylineno, filename);
}
void ETag_start(void)
{
  printf("%s\n} /* STag_%s */\n\n", pcdata, tag);
}

void STag_end(void)
{
  printf("void ETag_%s(void)\n{", tag = A_end_tag);
  printf("\n#line %d \"%s\"\n", yylineno, filename);
}
void ETag_end(void)
{
  printf("%s\n} /* ETag_%s */\n\n", pcdata, tag);
}

char mainmissing = 1;

void STag_main(void) {}
void ETag_main(void)
{
  printf("\n#line %d \"%s\"\n", yylineno, filename);
  printf("%s\n", pcdata);
  mainmissing = 0;
}

void STag_actions(void) {}
void ETag_actions(void) {
  if (mainmissing) {
    printf("/* Dummy main: filter XML from stdin. */\n");
    printf("int main() { exit(yylex()); }\n");
  }
}

int main(int argc, char** argv)
{
  filename = argv[1];
  yyin = fopen(filename,"r");
  return yylex();
}
