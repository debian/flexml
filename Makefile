# Make(1) rules for FleXML XML processor generator system.
# Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
#
# This file is part of the FleXML XML processor generator system.
# Copyright (c) 1999 Kristoffer Rose.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc., 59
# Temple Place, Suite 330, Boston, MA 02111-1307 USA.

# $Id: Makefile,v 1.47 2013/01/29 14:35:24 mquinson Exp $

# SUFF (defined in Makefile.defs) is the versioning suffix added to binaries and
# resource files. To get rid of it (and do a non-versioned install,
# for example), do:
#   make whatever SUFF=

# FILES.

include Makefile.defs

STUFF =	GPL Makefile Makefile.defs flexml.pl
BINS  =	$(FLEXML)
LIBS  =	$(FLEXML_ACT)
DATA  =	skel
MANS  =	flexml.1
DOCS  =	README ChangeLog NEWS NOTES TODO flexml-act.dtd
HTMLS =	FleXML.html paper.html

SRC =	$(STUFF) $(DATA) $(DOCS) $(HTMLS)
ALL =	$(PROGS) $(LIBS) $(DATA) $(MANS) $(DOCS) $(HTMLS)

.PHONY:		all install dist test clean

# PRIMARY TARGETS.

all:		$(ALL)

install:	$(ALL)
	mkdir -p $(DESTDIR)$(BINDIR) $(DESTDIR)$(LIBDIR) \
		$(DESTDIR)$(MAN1DIR) $(DESTDIR)$(DOCDIR) \
		$(DESTDIR)$(DATADIR) \
		$(DESTDIR)$(DOCDIR)/html \
		$(DESTDIR)$(DOCDIR)/examples
	$(INSTALL) -m555 $(BINS) $(DESTDIR)$(BINDIR)/
	$(INSTALL) -m555 $(LIBS) $(DESTDIR)$(LIBDIR)/
	$(INSTALL) -m444 $(DATA) $(DESTDIR)$(DATADIR)/
	$(INSTALL) -m444 $(MANS) $(DESTDIR)$(MAN1DIR)/
	$(INSTALL) -m444 $(DOCS) $(DESTDIR)$(DOCDIR)/
	$(INSTALL) -m444 $(HTMLS) $(DESTDIR)$(DOCDIR)/html/
	if test "x$(SUFF)" != "x" ; then \
	  rm -f $(DESTDIR)$(BINDIR)/flexml; \
	  ln -s $(DESTDIR)$(BINDIR)/$(FLEXML) $(DESTDIR)$(BINDIR)/flexml;\
	fi
	$(MAKE) -C examples install

FLEXML_DISTDIR = $(FLEXML)-$(VER)
dist:	test clean
	@echo "Building distribution..."
	mkdir $(FLEXML_DISTDIR)
	cp $(STUFF) $(DATA) $(DOCS) $(HTMLS) flexml-act-bootstrap.c \
		$(FLEXML_DISTDIR)
	cp -r examples testbed $(FLEXML_DISTDIR)
	find $(FLEXML_DISTDIR) -name CVS | xargs rm -rf
	find $(FLEXML_DISTDIR) -name .cvsignore | xargs rm -rf
	tar cvfz $(FLEXML_DISTDIR).tar.gz $(FLEXML_DISTDIR)
	rm -rf $(FLEXML_DISTDIR)

	#rsync -v FleXML.html $(WEBHOME)/FleXML.html
	#rsync -va --cvs-exclude --delete-excluded ./ $(FTPHOME)/

clean::; @echo "Cleaning..."
	$(RM) -rf $(FLEXML_DIR).tar.gz $(FLEXML_DIR)
	$(RM) *.[olh1] *-dummy.? lex.* *~ ./#*
	find -name '*~' | xargs $(RM)
test::		all
	@echo "Testing..."

clean::; $(RM) $(FLEXML) $(FLEXML_ACT) flexml-act flexml-act.c

$(FLEXML):		flexml.pl
	sed \
	    -e "s;FLEXMLVERSION;$(VER);g" \
	    -e "s;[.][/]flexml-act;$(ACT);g" \
	    -e "s;[.][/]skel;$(SKEL);g" \
	    -e "s;/var/tmp;$(TMPDIR);g" \
	    -e "s;/usr/share/doc/;$(DOCDIR)/;g" flexml.pl > $@
	chmod +x $@

ifneq ($(SUFF),)
$(FLEXML_ACT): flexml-act
	cp flexml-act $@
endif

# Action language...
flexml-act.l:		flexml-act.dtd skel
	$(PERL) ./flexml.pl $(FLEXDEBUG) -Lv -ractions -s skel $<

flexml-act.c:		flexml-act.l
	$(FLEX) -B -s -v -oflexml-act.c flexml-act.l

flexml-act.o:		flexml-act.c flexml-act.h
flexml-act-bootstrap.o:	flexml-act-bootstrap.c flexml-act.h
flexml-act:		flexml-act.o flexml-act-bootstrap.o

clean::; $(RM) TAGS
TAGS: 	$(SRC);	etags $(SRC)
ci:	$(SRC);	ci -u $(SRC)

# DOCUMENTS.

clean::; $(RM) flexml.html index.html pod2html-*

flexml.1:	$(FLEXML)
	$(POD2MAN) $(FLEXML) > $@

flexml.html:	$(FLEXML)
	$(POD2HTML) < $(FLEXML) > $@

index.html:	FleXML.html
	sed 's.ftp/FleXML/..g' FleXML.html > $@

# TESTS.

clean::; $(MAKE) -C examples $@; $(MAKE) -C testbed $@
test::;  $(MAKE) -C examples $@; \
		$(MAKE) -C testbed $@ > test.out 2>&1; ! grep '^fail' test.out && \
		echo "Tests succeeded."


# END.

clean::; @echo "Done cleaning."
test::;	@echo "Done testing."
